import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {


  // Member Variable
  url: string = 'http://localhost:80/api/gets'
  chartOption: any = {
    responsive: true
  }
  lastDate


  // Count
  countChartData: any[] = [ { data: [] }, { data: [] } ]
  countChartLabels: any[] = [
    'Tweets Count', 'Resplies Status Count', 'Resplies Chat Count', 'Resplies Tag Count', "Replieses Count", "All Count"
  ]


  // Sentiment
  sentimentChartData: any[] = [ { data: [] }, { data: [] } ]
  sentimentChartLabels: any[] = [
    'Tweets Sentiment', 'Replies Sentiment', "Replies Chat Sentiment", "Replies Tag Sentiment", "Replieses Average", "All Average"
  ]


  // Cycle Functions
  constructor(private http: HttpClient){

    // Fetch data from the server
    this.http.get(this.url)
    .subscribe((result: Informations) => {
      
      // Local Variable
      let ganjar = result.ganjar
      let sudirman = result.sudirman

      // Set Last Date
      this.lastDate = result.counted_at

      // Tweets Count, Replies Count, Chat Count, At Count
      this.setDataCountChart(ganjar, sudirman)

      // Replies Sentiment, Chat Sentiment, At Sentiment, Tweets Sentiments
      this.setDataSentimentChart(ganjar, sudirman)

    }, err => {
      console.log(err)
    })

  }


  // Private Functions
  private setDataCountChart(ganjar: Information, sudirman: Information){
  
    this.countChartData = [
      {data: [
        ganjar.tweets.count,
        ganjar.replies.to_status.count,
        ganjar.replies.to_user.count,
        ganjar.replies.to_at.count,
        ganjar.replies.count,
        ganjar.tweets.count + ganjar.replies.count
      ], label: "Ganjar Pranowo"},
      {data: [
        sudirman.tweets.count,
        sudirman.replies.to_status.count,
        sudirman.replies.to_user.count,
        sudirman.replies.to_at.count,
        sudirman.replies.count,
        sudirman.tweets.count + sudirman.replies.count
      ], label: "Sudirman Said"}
    ]
  
  }

  private setDataSentimentChart(ganjar: Information, sudirman: Information){
    
    // Local Variables
    let gReplies = ganjar.replies
    let sReplies = sudirman.replies

    let gtp = Number((ganjar.tweets.positive / (ganjar.tweets.positive + ganjar.tweets.negative)).toFixed(3))
    let gts = Number((gReplies.to_status.positive / (gReplies.to_status.positive + gReplies.to_status.negative)).toFixed(3))
    let gtu = Number((gReplies.to_user.positive / (gReplies.to_user.positive + gReplies.to_user.negative)).toFixed(3))
    let gta = Number((gReplies.to_at.positive / (gReplies.to_at.positive + gReplies.to_at.negative)).toFixed(3))

    let stp = Number((sudirman.tweets.positive / (sudirman.tweets.positive + sudirman.tweets.negative)).toFixed(3))
    let sts = Number((sReplies.to_status.positive / (sReplies.to_status.positive + sReplies.to_status.negative)).toFixed(3))
    let stu = Number((sReplies.to_user.positive / (sReplies.to_user.positive + sReplies.to_user.negative)).toFixed(3))
    let sta = Number((sReplies.to_at.positive / (sReplies.to_at.positive + sReplies.to_at.negative)).toFixed(3))

    let gar = ((gts + gtu + gta) / 3).toFixed(3)
    let sar = ((sts + stu + sta) / 3).toFixed(3)
    let ga = ((gta + gtu + gts + gtp) / 4).toFixed(3)
    let sa = ((sts + stu + sta + stp) / 4).toFixed(3)

    // Do
    this.sentimentChartData = [
      {data: [
        gtp, gts, gtu, gta, gar, ga
      ], label: 'Ganjar Pranowo'},
      {data: [
        stp, sts, stu, sta, sar, sa
      ], label: 'Sudirman Said'}
    ]

  }

}



// INTERFACES


interface Info{

  count: number
  max_id: number
  max_id_str: string
  positive: number
  negative: number
  last_tweet_date: string

}

interface Replies{
  
  to_user: Info
  to_status: Info
  to_at: Info
  count: number

}

interface Information{

  tweets?: Info
  replies?: Replies

}

interface Informations{

  ganjar: Information
  sudirman: Information
  counted_at?: string
  time_to_finish?: number

}
